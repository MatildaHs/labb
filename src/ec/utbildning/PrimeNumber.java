package ec.utbildning;

class PrimeNumber implements Runnable {
    int start;
    int end;

    public int getCount() {
        return count;
    }

    int count = 0;

    public PrimeNumber(int start, int end) {
        this.start = start;
        this.end = end;

    }

    @Override
    public void run() {

        for (int i = start; i <= end; i++) {
            if (checkPrime(i)) {

                count++;
            }

        }
        System.out.println(count);
    }

    public boolean checkPrime(int x) {

        for (int i = 2; i <= Math.sqrt(x); i++)
            if (x % i == 0) {
                return false;
            }
        return true;
    }
}
