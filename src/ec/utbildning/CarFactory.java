package ec.utbildning;

class CarFactory {
    public Car createCar(String carType) {

        if (carType.equals("BMW")) {
            return new BMW();
        } else if (carType.equals("Volvo")) {
            return new Volvo();
        }

        return null;
    }
    abstract static class Car {
        public Car() {
        }

        abstract void paintCar();


    }
    static class BMW extends Car{

        @Override
        void paintCar() {
            System.out.println("Painting car Red");
        }
    }



    static class Volvo extends Car {

        @Override
        void paintCar() {
            System.out.println("Painting car blue");
        }
    }
}
