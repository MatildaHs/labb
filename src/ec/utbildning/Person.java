package ec.utbildning;

class Person {
    private String name;
    private String gender;
    private Double salary;

    public String getName() {
        return name;
    }
    public String getGender() {
        return gender;
    }
    public Person(String name, String gender, Double salary) {
        this.name = name;
        this.gender = gender;
        this.salary = salary;
    }

    public double getSalary() {
        return salary;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                ", salary=" + salary +
                '}';
    }
}
