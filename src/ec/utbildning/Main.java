package ec.utbildning;
import java.util.*;
import java.util.Comparator;



public class Main {

    public static void main(String[] args) {
//1
        List<Person>person=List.of(
                new Person("Anita","Female",54000.0),
                new Person("Eva","Female",34000.0),
                new Person("Olle","Male",43000.0),
                new Person("Bengt","Female",32000.0),
                new Person("Lena","Female",23000.0),
                new Person("Peter","Male",73000.0),
                new Person("Maria","Female",47000.0),
                new Person("Bart","Male",12000.0),
                new Person("Alice","Female",18000.0),
                new Person("Erik","Male", 14000.0)
        );


//1.1
        double femaleAverage=Math.round(person.stream()
                .filter(x->x.getGender().equals("Female"))
                .mapToDouble(Person::getSalary)
                .summaryStatistics()
                .getAverage());
        System.out.println("Female average salary:"+femaleAverage);

        double maleAverage=Math.round(person.stream()
                .filter(x->x.getGender().equals("Male"))
                .mapToDouble(Person::getSalary)
                .summaryStatistics()
                .getAverage());
        System.out.println("Male average salary: "+maleAverage);


//1.2
        System.out.println("Most salary: ");
        person
                .stream()
                .sorted(Comparator.comparing(Person::getSalary))
                .skip(9)
                .forEach(System.out::println);
//1.3
        System.out.println("Lowest salary: ");
        person
                .stream()
                .sorted(Comparator.comparing(Person::getSalary).reversed())
                .skip(9)
                .forEach(System.out::println);
//2
        CarFactory carFactory=new CarFactory();
        CarFactory.Car volvo=carFactory.createCar("Volvo");
        volvo.paintCar();


 //3


        List<String>wordList= Arrays.asList("my","kitten","was","running","down","the","street",
                "It","was","looking","for","a","mouse","to","eat");
             wordList.stream()
                     .filter(word->word.replaceAll("[^aeiou]","").length()>=2)
                     .forEach(System.out::println);

  //4

        PrimeNumber primeNumber=new PrimeNumber(2,100);
        Thread thread = new Thread(primeNumber);
        PrimeNumber primeNumber2=new PrimeNumber(101,200);
        Thread threadTwo = new Thread((primeNumber2));

        thread.start();
        threadTwo.start();
        try {

            // join() method waits for the thread to die
            thread.join();
            threadTwo.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }
}
